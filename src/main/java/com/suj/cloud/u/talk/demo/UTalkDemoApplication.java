package com.suj.cloud.u.talk.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"com.suj.cloud"})
public class UTalkDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(UTalkDemoApplication.class, args);
    }
}
