package com.suj.cloud.u.talk.demo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

/**
 * @author jtoms
 */
@Slf4j
@RestController
public class IndexController {

    @GetMapping("/getToken")
    public String getToken(@RequestParam("userId") String userId) {
        String accessKeyId = "c9884e3e95c4000";
        String accessKeySecret = "AEVCGU2wAexiKHHtbr0pKwc4XPg91PbLzPeAZym1Xa6aio2qbPHK881J2m22tVSp";

        // 指定token过期时间
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 60);
        //建议每次会话都生成一个Token，保证系统的安全性
        return JWT.create()
                .withClaim("userId", userId)
                .withClaim("accessKeyId", accessKeyId)
                .withExpiresAt(calendar.getTime())
                .sign(Algorithm.HMAC256(accessKeySecret));
    }
}
